from datetime import datetime
import os
import numpy
import torch
from statistics import mean
import pandas
from postprocess.plotting import (
    plot_values,
    plot_times
)

agg_fn_dict = {
    'avg': mean,
    'size': len,
    'max': max,
    'min': min,
    'sum': sum,
    'last': lambda x: x[-1],
    'arrayElemAt': lambda x, y: x[y],
}


class DiskWriter(object):
    def __init__(self, results_dir, save_checkpoints, save_strategies, plot):
        self.data = {'date_created': datetime.now(),
                     'info': {}}
        self.id = None
        self.results_dir = results_dir
        self.save_checkpoints = save_checkpoints
        self.save_strategies = save_strategies
        self.plot = plot
        self.plots_dir = os.path.join(results_dir, 'plots')
        self.checkpoints_dir = os.path.join(results_dir, 'checkpoints')
        self.strategies_dir = os.path.join(results_dir, 'strategies')
        self.params = dict(results_dir=results_dir,
                           save_checkpoints=save_checkpoints,
                           save_strategies=save_strategies,
                           plot=plot)

    def add_info(self, variable_name, value):
        self.data['info'][variable_name] = value

    def add_meta(self, variable_name, value):
        self.data[variable_name] = value

    def view(self):
        return self.data

    def close(self):
        self.data['date_finished'] = datetime.now()


class DiskExperimentWriter(DiskWriter):
    def __init__(self, results_dir, *args, **kwargs):
        results_dir = results_dir.replace('{timestamp}', datetime.now().strftime('%y%h%d/%H-%M-%S'))
        results_dir = os.path.join(os.getcwd(), results_dir)
        os.makedirs(results_dir, exist_ok=True)
        super(DiskExperimentWriter, self).__init__(results_dir, *args, **kwargs)
        self.data['runs_df'] = {
            'info': pandas.DataFrame(),
            'plots': pandas.DataFrame(),
            'times': pandas.DataFrame(),
        }
        if self.plot:
            os.makedirs(self.plots_dir, exist_ok=True)
        if self.save_checkpoints:
            os.makedirs(self.checkpoints_dir, exist_ok=True)
        if self.save_strategies:
            os.makedirs(self.strategies_dir, exist_ok=True)

    def add_run(self, run):
        runs_df = self.data['runs_df']

        info_df = pandas.DataFrame([run['info']]).set_index('epoch')
        runs_df['info'] = pandas.concat([runs_df['info'], info_df])

        plots_df = get_indexed_df(run['info']['epoch'], run['plots'])
        runs_df['plots'] = pandas.concat([runs_df['plots'], plots_df])

        times_df = get_indexed_df(run['info']['epoch'], run['times'])
        runs_df['times'] = pandas.concat([runs_df['times'], times_df])

    def aggregate_stats(self, aggregations):
        for var, new_var, agg_fn, *params in aggregations:
            key, var = var.split('.')
            self.data['info'][new_var] = agg_fn_dict[agg_fn](self.data['runs_df'][key][var], *params)

    def close(self):
        runs_df = self.data['runs_df']
        runs_df['info'].sort_index(inplace=True)
        runs_df['info'].to_csv(os.path.join(self.results_dir, 'stats_epoch.csv'))
        runs_df['plots'].to_csv(os.path.join(self.results_dir, 'stats_full.csv'))
        runs_df['times'].to_csv(os.path.join(self.results_dir, 'times_full.csv'))
        info_series = pandas.Series(self.data['info'])
        info_series.to_csv(os.path.join(self.results_dir, 'stats_final.csv'), header=False)

        if self.plot:
            plots = {key: key for key in self.data['runs_df']['plots'].columns}
            plot_values(self.data['runs_df']['plots'].reset_index(),
                        os.path.join(self.plots_dir, 'training_progress.pdf'), plots=plots)
            plot_times(self.data['runs_df']['times'].reset_index(),
                       os.path.join(self.plots_dir, 'avg_times.pdf'))

        super(DiskExperimentWriter, self).close()
        return self.data['info']


class DiskRunWriter(DiskWriter):
    def __init__(self, epoch, *args, **kwargs):
        super(DiskRunWriter, self).__init__(*args, **kwargs)
        self.id = epoch

    def add_scalar(self, variable_name, value):
        key, variable_name = variable_name.split('.')
        self.data.setdefault(key, {}).setdefault(variable_name, []).append(value)

    def add_params(self, params):
        self.data['params'] = params

    def add_strategy(self, strategy, state_dict):
        self.data['strategy'] = strategy
        self.data['state_dict'] = state_dict

    def aggregate_stats(self, aggregations):
        for var, new_var, agg_fn, *params in aggregations:
            key, var = var.split('.')
            self.data['info'][new_var] = agg_fn_dict[agg_fn](self.data[key][var], *params)

    def close(self):
        # get_indexed_df(self.id, self.data['plots']).to_csv(os.path.join(self.plots_dir, f'plots_{self.id:02d}.csv'))
        # get_indexed_df(self.id, self.data['times']).to_csv(os.path.join(self.plots_dir, f'times_{self.id:02d}.csv'))
        if self.save_checkpoints:
            torch.save(self.data['state_dict'],
                       os.path.join(self.checkpoints_dir, f'strategy_sd_{self.id:02d}.pth'))
        if self.save_strategies:
            numpy.save(os.path.join(self.strategies_dir, f'strategy_{self.id:02d}.npy'),
                       self.data['strategy'], allow_pickle=True)

        super(DiskRunWriter, self).close()
        return self.data


def get_indexed_df(epoch, data_dict):
    df = pandas.DataFrame(data_dict)
    df['step'] = range(1, len(df) + 1)
    df['epoch'] = epoch
    return df.set_index(['epoch', 'step'])


run_writers_dct = {
    'disk': DiskRunWriter,
}

experiment_writers_dct = {
    'disk': DiskExperimentWriter,
}
