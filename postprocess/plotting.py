import math
import os
import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
from itertools import product, chain, combinations
from math import ceil


def plot_values(log_data, filename, plots=None):
    defaults = dict(val='value', val_train='training value', loss='loss')
    plots = plots or defaults
    fig, axes = plt.subplots(len(plots), 1, figsize=(10, 5 * len(plots)))

    for (key, ylabel), ax in zip(plots.items(), axes):
        ax.set(ylabel=ylabel or key)
        ax.set_ylim([None, 4 * log_data[key].median()])  # hack: cut very high values
        data = log_data.pivot(index='step', columns='epoch', values=key)
        data.plot(ax=ax, legend=False, grid=True, lw=0.5)

    fig.savefig(filename)


def plot_statistics(epoch_data, filename):
    fig, ((ax_best_val, ax_avg_val), (ax_best_val_at, ax_steps), (ax_time, ax_step_time)) = plt.subplots(3, 2, figsize=(
        10, 15))

    def stats_dict(key):
        return dict(mean=epoch_data[key].mean(),
                    std=epoch_data[key].std(),
                    median=epoch_data[key].median(),
                    max=epoch_data[key].max(),
                    min=epoch_data[key].min())

    str_fmt = '\n{min:.1f} ≤ {median:.1f} / {mean:.1f} ± {std:.1f} ≤ {max:.1f}'

    plots = dict(best_val=(ax_best_val, 'Best value', '#1f77b4'),
                 avg_val=(ax_avg_val, 'Average value', '#1f77b4'),
                 best_val_at=(ax_best_val_at, 'Best value at', '#2ca02c'),
                 steps=(ax_steps, 'Steps', '#2ca02c'),
                 epoch_time=(ax_time, 'Epoch time', '#ff7f0e'),
                 avg_step_time=(ax_step_time, 'Average step time', '#ff7f0e'))

    for key, (ax, title, color) in plots.items():
        stats = stats_dict(key)
        ax.set(title=title + str_fmt.format(**stats))
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        epoch_data[key].hist(ax=ax, legend=False, grid=False,
                             edgecolor='k', alpha=0.7, color=color,
                             bins=ceil(epoch_data[key].nunique() / 4.0))
        ax.axvline(stats['median'], color='k', linewidth=1)
        ax.axvline(stats['mean'], color='k', linestyle='dashed', linewidth=1)

    fig.savefig(filename)


def plot_times(log_data, filename):
    max_steps = log_data['step'].max()
    fig, ax_time = plt.subplots(figsize=(6 + max_steps / 20, 6))
    ax_time.set(xlabel='steps', ylabel='seconds', title='Average execution times')

    means = dict(forward_time=('forward', 'mean'),
                 backward_time=('backward', 'mean'),
                 optimizer_time=('optimizer', 'mean'),
                 eval_time=('eval', 'mean'))
    stds = dict(forward_time=('forward', 'std'),
                backward_time=('backward', 'std'),
                optimizer_time=('optimizer', 'std'),
                eval_time=('eval', 'std'))
    times_avg = log_data.groupby('step').aggregate(**means)
    times_std = log_data.groupby('step').aggregate(**stds)
    # fill nans if epochs = 1:
    times_std = times_std.fillna(0)
    elinewidth = min(100 / max_steps, 1)
    times_avg.plot.bar(ax=ax_time, stacked=True, yerr=times_std, lw=0, zorder=3,
                       error_kw=dict(elinewidth=elinewidth))
    ticks = [10 * i for i in range(1, ceil(max_steps / 10))]
    ax_time.set_xticks(ticks=[i - 1 for i in ticks])
    ax_time.set_xticklabels(labels=ticks, rotation=0)
    ax_time.grid(zorder=0)
    ax_time.legend(['forward', 'backward', 'optimizer', 'eval'])
    fig.savefig(filename)


def is_logscale(par):
    return par['distribution'].__contains__('log')


def is_categorical(par):
    return par['distribution'].__contains__('grid') or par['distribution'].__contains__('choice')


def tune_plot_metrics(df, df_epoch, params, metrics, results_dir='', save=True, filename='plot_metrics.pdf'):
    """
    @param df: Pandas DataFrame of ray.tune results
    @param df_epoch:
    @param metrics: reported metrics to plot
    @param params: optimized_params dictionary
    @param save:
    @param results_dir: path to store the plots
    @param filename: name of the plot (.pdf or .png)
    @return: matplotlib's figure
    """
    metrics_n = len(metrics)
    params_n = len(params)

    fig, axis = plt.subplots(metrics_n, params_n, figsize=(params_n * 8, metrics_n * 5), squeeze=False)
    for (metric, par), ax in zip(product(metrics, params.keys()), chain(*axis)):
        ax.set(ylabel=metric, xlabel=par)
        if is_logscale(params[par]):
            ax.set(xscale='log')
        ax.scatter(df_epoch.index.get_level_values(par), df_epoch[metric], alpha=0.3)
        ax.scatter(df['config.' + par], df[metric], alpha=1.0, marker='x')

    fig.tight_layout()
    if save:
        fig.savefig(os.path.join(results_dir, filename))
    return fig


def tune_plot_metrics_stats(df, df_epoch, params, metrics, results_dir='', save=True,
                            filename='plot_metrics_stats.pdf'):
    metrics_n = len(metrics)
    params_n = len(params)
    levels = dict(zip(df_epoch.index.names, df_epoch.index.levels))

    fig, axis = plt.subplots(metrics_n, params_n, figsize=(params_n * 8, metrics_n * 5), squeeze=False)
    for (metric, par), ax in zip(product(metrics, params.keys()), chain(*axis)):
        if is_logscale(params[par]):
            ax.set(xscale='log')
        if metric in df_epoch and is_categorical(params[par]):
            width = (max(levels[par]) - min(levels[par])) / (2 * len(levels[par]))
            df_epoch.boxplot(ax=ax, by=[par], column=[metric], positions=levels[par], widths=width)
        df.groupby('config.' + par).plot.scatter(ax=ax, x='config.' + par, y=metric)
        ax.set(ylabel=metric, xlabel=par)
        ax.grid(linestyle='-', linewidth=0.3)
        ax.set_title('')

    fig.suptitle('')
    fig.tight_layout()
    if save:
        fig.savefig(os.path.join(results_dir, filename))
    return fig


def tune_plot_pairwise_dependencies(df, df_epoch, params, metrics, results_dir='', save=True,
                                    filename='plot_pairwise.pdf'):
    metrics_n = len(metrics)
    params = dict(params)
    params_n = len(params)
    fig = None
    if params_n >= 2:
        ax_n = params_n * (params_n - 1) // 2
        fig, axis = plt.subplots(metrics_n, ax_n, figsize=(ax_n * 5, metrics_n * 5), squeeze=False)
        for metric, ax_m in zip(metrics, axis):
            for (p1, p2), ax in zip(combinations(params.keys(), 2), ax_m):
                ax.set(xlabel=p1, ylabel=p2)
                if is_logscale(params[p1]):
                    ax.set(xscale='log')
                if is_logscale(params[p2]):
                    ax.set(yscale='log')
                scatter = ax.scatter(df['config.' + p1], df['config.' + p2], c=df[metric])
            ax.legend(*scatter.legend_elements(),
                      title=metric, bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0)

        fig.tight_layout()
        if save:
            fig.savefig(os.path.join(results_dir, filename))
    return fig


def tune_plot_single_parallel_coordinates(df, params, metric, results_dir='', save=True, filename='plot_parallel.pdf'):
    fig = None
    if len(params) >= 2:
        line = dict(
            color=df[metric],
            colorscale='Tealrose',
            showscale=True,
            colorbar=dict(
                title=dict(text=metric, side='bottom')
            )
        )
        dimensions = []
        for param in params.keys():
            col = 'config.' + param
            if is_logscale(params[param]):
                values = np.log10(df[col])
                min_value = values.min()
                max_value = values.max()
                tickvals = list(range(math.ceil(min_value), math.ceil(max_value)))
                if min_value not in tickvals:
                    tickvals = [min_value] + tickvals
                if max_value not in tickvals:
                    tickvals = tickvals + [max_value]
                dim = dict(
                    label=param,
                    values=values,
                    range=(values.min(), values.max()),
                    tickvals=tickvals,
                    ticktext=["{:.3g}".format(math.pow(10, x)) for x in tickvals]
                )
            else:
                dim = dict(
                    label=param,
                    values=df[col],
                    range=(df[col].min(), df[col].max())
                )
            dimensions.append(dim)
        fig = go.Figure(go.Parcoords(line=line, dimensions=dimensions))
        if save:
            fig.write_image(os.path.join(results_dir, filename))
    return fig


def tune_plot_parallel_coordinates(df, df_epoch, params, metrics, results_dir='', save=True, filename='plot_parallel'):
    for metric in metrics:
        tune_plot_single_parallel_coordinates(df, params, metric, results_dir=results_dir, save=save,
                                              filename=f'{filename}_{metric}.pdf')


def tune_plot_pareto_points(df, df_epoch, params, metrics, results_dir='', save=True, filename='plot_pareto.pdf'):
    metric_x = 'cost_std'
    metric_y = 'cost_e'
    par = 'game_params.beta'
    assert metric_x in metrics and metric_y in metrics and par in params

    fig, ax = plt.subplots()
    ax.scatter(df_epoch[metric_x], df_epoch[metric_y], c=df_epoch.index.get_level_values(par),
               alpha=0.2, marker='x', s=10, linewidths=0.5)
    scatter = ax.scatter(df[metric_x], df[metric_y], c=df['config.' + par], marker='o', s=10)
    ax.legend(*scatter.legend_elements(), title='beta', bbox_to_anchor=(0.99, 0.99), loc="upper right", borderaxespad=0)
    ax.set(xlabel='standard deviation', ylabel='expectation')
    ax.grid(linestyle='-', linewidth=0.3)
    fig.tight_layout()

    if save:
        fig.savefig(os.path.join(results_dir, filename))
    return fig


tune_plotting_dict = {
    'plot_metrics': tune_plot_metrics,
    'plot_metrics_stats': tune_plot_metrics_stats,
    'plot_pairwise_dependencies': tune_plot_pairwise_dependencies,
    'plot_parallel_coordinates': tune_plot_parallel_coordinates,
    'plot_pareto_points': tune_plot_pareto_points
}
