import os
from time import process_time
from writers import run_writers_dct, experiment_writers_dct

mode_dct = {
    'min': -1,
    'max': 1
}


class ExperimentReporting(object):
    def __init__(self, database, database_params, settings,  mode, tag=None, description=None, verbose=False):
        self.mode = mode
        self.verbose = verbose
        self.writer = experiment_writers_dct[database](**database_params)
        self.writer.add_meta('settings', settings)
        self.writer.add_meta('tag', tag)
        self.writer.add_meta('description', description)
        self.writer.add_meta('script', os.path.basename(__file__))

        self.params = {'database': database,
                       'database_params': self.writer.params,
                       'mode': self.mode,
                       'verbose': verbose,
                       'tag': tag,
                       'experiment_id': self.writer.id}

        self.start_time = process_time()

    def update(self, run):
        self.writer.add_run(run)

    def dump(self):
        self.writer.add_info('total_time', process_time() - self.start_time)

        aggregations = [
            ('info.best_val', 'best_val', self.mode),
            # todo: best_val_at_epoch
            # ('info.epoch', 'best_val_at_epoch', 'argmax/argmin'),  #not defined!!
            ('info.epoch_time', 'avg_epoch_time', 'avg'),
        ]
        aggregations += [(f'info.avg_{x}_time', f'avg_{x}_time', 'avg') for x in
                         ['forward', 'backward', 'optimizer', 'eval', 'step']]
        self.writer.aggregate_stats(aggregations)

        experiment_stats = self.writer.close()

        if self.verbose:
            self.print_stats(experiment_stats)
        return experiment_stats

    def print_stats(self, stats):
        str_fmt = '-------------'
        str_fmt += '\nbest val\t= {best_val:.3f}'
        # str_fmt += '\nbest val\t= {best_val:.1f} at epoch = {best_val_at_epoch}'
        str_fmt += '\navg epoch time\t= {avg_epoch_time:.3f}'
        str_fmt += '\navg step time\t= {avg_step_time:.3f}'
        str_fmt += '\ntotal time\t= {total_time:.1f}'
        print(str_fmt.format(**stats))


class RunReporting(object):
    def __init__(self, epoch, database, database_params, tag, experiment_id, mode, verbose):
        epoch += 1
        self.mode = mode
        self.verbose = verbose
        self.writer = run_writers_dct[database](epoch, **database_params)
        self.writer.add_meta('tag', tag)
        self.writer.add_meta('experiment', experiment_id)
        self.writer.add_info('epoch', epoch)

        self.best_val = - mode_dct[self.mode] * float('inf')
        self.best_at = 0
        self.extras = set()
        self.start_time = process_time()

    def update(self, step_num, val, val_train, loss,
               strategy, state_dict,
               f_time, b_time, o_time, e_time,
               extras=None):

        self.writer.add_scalar('plots.val', val)
        self.writer.add_scalar('plots.val_train', val_train)
        self.writer.add_scalar('plots.loss', loss)
        self.writer.add_scalar('times.forward', f_time)
        self.writer.add_scalar('times.backward', b_time)
        self.writer.add_scalar('times.optimizer', o_time)
        self.writer.add_scalar('times.eval', e_time)
        self.writer.add_scalar('times.step', f_time + b_time + o_time + e_time)

        extras = extras or {}
        for key, v in extras.items():
            self.extras.add(key)
            self.writer.add_scalar(f'plots.{key}', v)

        if mode_dct[self.mode] * val > mode_dct[self.mode] * self.best_val:
            self.best_at = step_num
            self.writer.add_info('best_val', val)
            self.writer.add_info('best_val_at', step_num)
            self.writer.add_strategy(strategy, state_dict)

    def dump(self):
        self.writer.add_info('epoch_time', process_time() - self.start_time)
        aggregations = [
            # ('plots.val', 'best_val', self.mode),  # not needed, already in info.best_val
            ('plots.val', 'end_val', 'last')
        ]
        aggregations += [(f'plots.{key}', key, 'arrayElemAt', self.best_at - 1) for key in self.extras]
        aggregations += [(f'times.{x}', f'avg_{x}_time', 'avg') for x in
                         ['forward', 'backward', 'optimizer', 'eval', 'step']]
        self.writer.aggregate_stats(aggregations)

        if self.verbose:
            self.print_run(self.writer.view()['info'])
        # returns either id for 'mongo' or data for 'disk'
        return self.writer.close()

    def print_run(self, run_info):
        str_fmt = 'epoch {epoch}:'
        str_fmt += '\tend val={end_val:.1f}'
        str_fmt += '\tbest val={best_val:.1f} at {best_val_at}'
        for key in self.extras:
            str_fmt += f'\t{key}={{{key}:.1f}}'
        # str_fmt += '\tavg_val={avg_val:.1f}'
        str_fmt += '\ttime={epoch_time:.2f}'
        str_fmt += '\tavg step time={avg_step_time:.3f}'
        print(str_fmt.format(**run_info))
