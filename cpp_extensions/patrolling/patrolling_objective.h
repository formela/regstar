#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <vector>
#include <algorithm>
#include <tuple>

#include "utils.h"

struct ADJ
//information about an adjacent vertex in a predecessor/successor list
{
	int To;
	double Length;
};

struct STRATEGY
//structure describing a strategy (note that some edges
//of the graph may be unused by the strategy)
{
	ARRAY2D<double, MAXQ, MAXQ> Prob; //Prob[i][j] is the probability of going to state j when being at state i
	ARRAY1D<int, MAXQ> OutDegree; //OutDegree[i] is the number of actual successors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Succ; //Succ[i][j] is the j-th actual successor of state i
	ARRAY1D<int, MAXQ> InDegree; //InDegree[i] is the number of actual predecessors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Pred; //Pred[i][j] is the j-th actual predecessor of state i

	ARRAY1D<int, MAXQ> IsReachable; //whether each state is reachable
	ARRAY1D<double, MAXQ> MaxInLength; //MaxInLength[i] is the maximum length of an actual edge going to state i
};


struct GRAPH
{
	struct PAR_MANAGER_CALLER
	{
		PAR_MANAGER_CALLER(GRAPH* G,
		                   const std::vector<int>& memory,
		                   int g)
		{
			ParManager.Init(G, sizeof(GRAPH), memory, g);
		}
	}
	PMC;

	int n; //number of vertices
	int q; //number of states
	int g; //number of targets
	ARRAY1D<double, MAXG> Time; //Time[i] is the time needed to perform a successful attack at vertex i
	ARRAY1D<double, MAXG> Weight; //Weight[i] is the value of target i
	ARRAY1D<int, MAXN> MemorySize; //MemorySize[i] is the memory size of vertex i

	ARRAY1D<double, MAXG> Blindness; //Blindess[t] is the probability of NOT detecting an ongoing attack in t when visiting t

#define INFTY ((double) (1ULL << 63))
	ARRAY2D<double, MAXN, MAXN> VEdgeLength; //INFTY = no edge

	double MaxWeight; //maximum of Weight[i] over all targets i
	double OrigMaxWeight; //in case the above gets normalized to 1.
	int tMaxWeight; //index of the most valued target

	ARRAY2D<double, MAXN, MAXN> VDist; //for Floyd-Warshall


	ARRAY1D<int, MAXN> VInDegree; //VInDegree[i] is the number of predecessors of vertex i
	ARRAY2D<ADJ, MAXN, MAXN> VPred; //VPred[i][j] is the j-th predecessor of vertex i
	ARRAY1D<int, MAXN> VOutDegree; //VOutDegree[i] is the number of successors of vertex i
	ARRAY2D<ADJ, MAXN, MAXN> VSucc; //VSucc[i][j] is the j-th successor of vertex i

	ARRAY1D<int, MAXQ> Q2V; //Q2V[i] is the vertex to which state i corresponds
	ARRAY1D<int, MAXN> V2Q; //V2Q[i] is the first state which corresponds to vertex i
	ARRAY1D<int, MAXQ> GlobOutDegree; //GlobOutDegree[i] is the number of successors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> GlobSucc; //GlobSucc[i][j] is the j-th successor of state i

	ARRAY1D<int, MAXQ> Stamp; //the time stamp for cycle search
	ARRAY1D<int, MAXQ> Q; //queue for reachability BFS
	int QBeg, QEnd;

	void Unfold(int i, int& v, int& e);
	void InitVSucc(bool Check);
	void InitFM();
	void FloydWarshall(double bonus);
	double GetBlindness(int t);

	void InitStrategyAdj();
	void FindReachableStates();
	void InitMaxInLength();

	GRAPH(std::vector<int> memory,
	      std::vector<double> time,
	      std::vector<double> weight,
	      std::vector<double> blindness,
	      std::vector<EDGE_TYPE> edge):
	        PMC(this, memory, time.size())
	{
		ParManager.InitFinished();
		this->Init(memory, time, weight, blindness, edge);
	}

	void Init(const std::vector<int>& memory,
	          const std::vector<double>& time,
	          const std::vector<double>& weight,
	          const std::vector<double>& blindness,
	          const std::vector<EDGE_TYPE>& edge);

	void LoadStrategy(at::Tensor /*double[Q][Q]*/ strat, bool CareForReachability = 1);

	at::Tensor /*double[G][Q]*/ PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat, double pmt);
	at::Tensor /*double[Q][Q]*/ PyComputeGradient(at::Tensor /*double[G][Q]*/ imps);
	std::pair<int, int> PyGetProblematicEdge();

	STRATEGY S;


	struct HEAPITEM
	{
		double d; //distance towards the examined target
		double p; //probability of the corresponding path(s)
		int i; //initial state
		int id; //id of this heap-item

		bool operator < (const HEAPITEM& other) const
		{
			return this->d > other.d;
		}
	};

	std::vector<HEAPITEM> Heap;
	ARRAY1D<std::vector<HEAPITEM>, MAXG> HeapList;
	ARRAY1D<std::vector<int>, MAXG> LabelO2N;
	ARRAY1D<int, MAXG> nLabels;
	ARRAY1D<std::vector<std::pair<int, int> >, MAXG> Contrib;
	std::vector<double> PD; //PD[i] is the partial derivative of the examined
	    //loss function with respect to HeapList[t][i].p for the currently
	    //examined target t

	ARRAY2D<double, MAXG, MAXQ> Catch; //Catch[t][i] is the probability of covering an attack at target t when being at state i,
	        //Catch[t][i] = 2. means that state i is unreachable by the defender (thus the attacker does not have this opportunity at all)

	ARRAY1D<std::pair<int, double>, MAXQ> CurListIndex;

	void ComputeValue(double pmt);
	void Search(int t, double pmt);

	std::vector<double> TotalPDForEdge; //the index is the edge
	void ComputeGradient(int t);
	ARRAY2D<double, MAXG, MAXQ> ImpArray; //ImpArray[t][i] is the importance coefficient of an attack at target t when being at state i

	ARRAY2D<int, MAXQ, MAXQ> EdgeToIndex;
	std::vector<std::pair<int, int>> IndexToEdge;

	struct PREFIXITEM
	{
		double d; //distance towards the examined target
		double p; //total probability of the corresponding path(s)

		bool operator < (const PREFIXITEM& other) const
		{
			return this->d < other.d;
		}
	};

	ARRAY1D<std::vector<PREFIXITEM>, MAXQ> PrefixSum;
	ARRAY1D<int, MAXQ> PrefixCount;

	void ComputeBeforeSwitch(GRAPH& G2, int t, double& SwitchLoss, double pmt);
	void AccountSwitchingOpportunity(const HEAPITEM& cur, int t, GRAPH& G2, double& catchprob, double& primarycatchprob);
	void ComputeSwitchingPrefixSums(int t);
};


struct SWITCHING_GRAPH
{
	GRAPH G1;
	GRAPH G2;

	SWITCHING_GRAPH(std::vector<int> memory,
	                std::vector<double> time,
	                std::vector<double> weight1,
	                std::vector<double> blindness1,
	                std::vector<EDGE_TYPE> edge1,
	                std::vector<double> weight2,
	                std::vector<double> blindness2,
	                std::vector<EDGE_TYPE> edge2):
	        G1(memory, time, weight1, blindness1, edge1),
	        G2(memory, time, weight2, blindness2, edge2)
	{
	}

	double PyEvaluateSwitchingStrategy(at::Tensor /*double[Q][Q]*/ strat1, at::Tensor /*double[Q][Q]*/ strat2, double pmt);
};


void GRAPH::LoadStrategy(at::Tensor /*double[Q][Q]*/ strat, bool CareForReachability /*= 1*/)
{
	if((strat.dim() != 2) || (strat.size(0) != q) || (strat.size(1) != q))
	{
		throw std::runtime_error("LoadStrategy: q*q array expected");
	}

	double* p = strat.data_ptr<double>();
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			S.Prob[i][j] = *(p++);
		}
	}

	InitStrategyAdj();

	if(CareForReachability)
	{
		FindReachableStates();
		InitMaxInLength();
	}
	else
	{
		for(int i = 0; i < q; i++)
		{
			S.IsReachable[i] = 1;
			S.MaxInLength[i] = 0.;
		}
	}
}


at::Tensor /*double[G][Q]*/ GRAPH::PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat, double pmt)
{
	LoadStrategy(strat);
	ComputeValue(pmt);

	at::Tensor rsl = torch::empty({g, q}, torch::kFloat64);
	double* p = rsl.data_ptr<double>();
	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			*(p++) = (S.IsReachable[i]) ? (1. - Catch[t][i]) : -1.;
		}
	}

	return rsl;
}


at::Tensor /*double[Q][Q]*/ GRAPH::PyComputeGradient(at::Tensor /*double[G][Q]*/ imps)
{
	if((imps.dim() != 2) || (imps.size(0) != g) || (imps.size(1) != q))
	{
		throw std::runtime_error("PyComputeGradient: invalid size of the received array");
	}

	double* impp = imps.data_ptr<double>();

	at::Tensor rsl = torch::zeros({q, q}, torch::kFloat64);
	double* p = rsl.data_ptr<double>();

	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			ImpArray[t][i] = *(impp++);
			if(!(S.IsReachable[i]))
			{
				ImpArray[t][i] = 0.;
			}
		}
	}

	IndexToEdge.clear();
	for(int a = 0; a < q; a++)
	{
		for(int b = 0; b < q; b++)
		{
			EdgeToIndex[a][b] = IndexToEdge.size();
			if(S.Prob[a][b] > 0.)
			{
				IndexToEdge.push_back({a, b});
			}
		}
	}

	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			if(ImpArray[t][i] != 0.)
			{
				goto MUST_COMPUTE_IT;
			}
		}

		continue;
        MUST_COMPUTE_IT:
		ComputeGradient(t);

		int nEdges = IndexToEdge.size();
		for(int e = 0; e < nEdges; e++)
		{
			auto& [a, b] = IndexToEdge[e];
			p[(a * q) + b] -= TotalPDForEdge[e];
		}
	}

	return rsl;
}


void Minimize(double& a, double b);
void Maximize(double& a, double b);



void GRAPH::FloydWarshall(double bonus)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			VDist[i][j] = VEdgeLength[i][j] - bonus;
		}

		Minimize(VDist[i][i], 0.);
	}

	for(int k = 0; k < n; k++)
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				Minimize(VDist[i][j], VDist[i][k] + VDist[k][j]);
			}
		}
	}
}


void GRAPH::Unfold(int i, int& v, int& e)
//unfolds state i to the corresponding vertex and memory element
{
	v = Q2V[i];
	int diff = i - V2Q[v];
	e = diff % MemorySize[v];
}


void GRAPH::InitVSucc(bool Check)
{
	for(int i = 0; i < n; i++)
	{
		VInDegree[i] = 0;
		VOutDegree[i] = 0;
	}

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double l = VEdgeLength[i][j];
			if(l < INFTY)
			{
				VPred[j][VInDegree[j]++] = (ADJ) {i, l};
				VSucc[i][VOutDegree[i]++] = (ADJ) {j, l};
			}
		}
	}

	if(Check)
	{
		for(int i = 0; i < n; i++)
		{
			assert(VOutDegree[i]);
		}
	}
}


void GRAPH::InitFM()
//initializes the arrays Q2V, V2Q, OutDegree, Succ
{
	q = 0;
	for(int i = 0; i < n; i++)
	{
		V2Q[i] = q;
		for(int j = 0; j < MemorySize[i]; j++)
		{
			Q2V[q++] = i;
		}
	}

//here you can hard-code the automaton part (or any restrictions to it)
	for(int i = 0; i < q; i++)
	{
		int v1, e1;
		Unfold(i, v1, e1);

		for(int j = 0; j < q; j++)
		{
			int v2, e2;
			Unfold(j, v2, e2);

			if(VEdgeLength[v1][v2] < INFTY)
			{
				GlobSucc[i][GlobOutDegree[i]++] = (ADJ) {j, VEdgeLength[v1][v2]};
			}
		}
	}
}


void GRAPH::InitStrategyAdj()
{
	for(int i = 0; i < q; i++)
	{
		S.OutDegree[i] = 0;
		S.InDegree[i] = 0;
	}

	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			if(S.Prob[i][j] > 0.)
			{
				S.Succ[i][S.OutDegree[i]++] = (ADJ) {j, VEdgeLength[Q2V[i]][Q2V[j]]};
			        S.Pred[j][S.InDegree[j]++]  = (ADJ) {i, VEdgeLength[Q2V[i]][Q2V[j]]};
		        }
	        }
        }
}


void GRAPH::FindReachableStates()
{
	for(int i = 0; i < q; i++)
	{
		S.IsReachable[i] = 0;
	}

	int v = V2Q[tMaxWeight];
	while(!(S.IsReachable[v]))
	//this should solve the problem of starting
	//in a state which is not reachable again
	{
		S.IsReachable[v] = 1;
		v = S.Succ[v][0].To;
	}

	for(int i = 0; i < q; i++)
	{
		S.IsReachable[i] = 0;
	}

	QBeg = 0;
	QEnd = 0;
	Q[QEnd++] = v;
	S.IsReachable[v] = 1;

	while(QBeg < QEnd)
	{
		int cur = Q[QBeg++];
		for(int k = 0; k < S.OutDegree[cur]; k++)
		{
			int s = S.Succ[cur][k].To;
			if(S.IsReachable[s])
			//already visited
			{
				continue;
			}

			Q[QEnd++] = s;
			S.IsReachable[s] = 1;
		}
	}
}


void GRAPH::InitMaxInLength()
{
	for(int i = 0; i < q; i++)
	{
		S.InDegree[i] = 0;
		S.MaxInLength[i] = 0.;
	}

	for(int i = 0; i < q; i++)
	{
		if(!(S.IsReachable[i]))
		{
			continue;
		}

		for(int k = 0; k < S.OutDegree[i]; k++)
		{
			int j = S.Succ[i][k].To;
			double l = S.Succ[i][k].Length;

			S.Pred[j][S.InDegree[j]++] = (ADJ) {i, l};
			Maximize(S.MaxInLength[j], l);
		}
	}
}


double GRAPH::GetBlindness(int t)
{
	return Blindness[t];
}


void GRAPH::ComputeValue(double pmt)
{
	long long TotalPV = 0;
	long long TotalPE = 0;
	for(int t = 0; t < g; t++)
	{
		Search(t, pmt);
		TotalPE += Contrib[t].size();
		TotalPV += HeapList[t].size();
	}
}


void GRAPH::Search(int t, double pmt)
{
	Heap.clear();
	HeapList[t].clear();

	LabelO2N[t].clear();
	nLabels[t] = 0;
	Contrib[t].clear();

	for(int i = 0; i < q; i++)
	{
		Catch[t][i] = 0.;

		if(Q2V[i] == t)
		{
			LabelO2N[t].push_back(nLabels[t]);
			Heap.push_back((HEAPITEM) {0., 1. - GetBlindness(t), i, nLabels[t]++});
		}
	}

	for(int i = 0; i < q; i++)
	{
		CurListIndex[i] = std::make_pair(-1, -INFTY);
	}

	while(!(Heap.empty()))
	{
		int iList = HeapList[t].size();
		double dLast;

		while(1)
		{
			HEAPITEM cur = Heap[0];
			dLast = cur.d;
			std::pop_heap(Heap.begin(), Heap.end());
			Heap.pop_back();

			if(cur.d <= CurListIndex[cur.i].second + pmt)
			{
				CurListIndex[cur.i].second = cur.d;
				LabelO2N[t][cur.id] = CurListIndex[cur.i].first;
				HeapList[t][CurListIndex[cur.i].first].d = cur.d;
				HeapList[t][CurListIndex[cur.i].first].p += cur.p;
			}
			else
			{
				CurListIndex[cur.i] = {HeapList[t].size(), cur.d};
				LabelO2N[t][cur.id] = HeapList[t].size();
				HeapList[t].push_back(cur);
			}

			if((Heap.empty()) || (Heap[0].d > cur.d + pmt))
			{
				break;
			}
		}

		for(; iList < (int) HeapList[t].size(); iList++)
		{
			HEAPITEM cur = HeapList[t][iList];
			cur.d = dLast;
			int i = cur.i;
			Catch[t][i] += cur.p;

			for(int k = 0; k < S.InDegree[i]; k++)
			{
				int j = S.Pred[i][k].To;

				double blind = 1.;
				if(Q2V[j] == t)
				{
					if((blind = GetBlindness(t)) == 0.)
					{
						continue;
					}
				}

				double d = cur.d + S.Pred[i][k].Length;

				if(d + S.MaxInLength[j] <= Time[t])
				{
					Contrib[t].push_back({cur.id, nLabels[t]});
					LabelO2N[t].push_back(nLabels[t]);
					Heap.push_back((HEAPITEM) {d, cur.p * S.Prob[j][i] * blind, j, nLabels[t]++});
					std::push_heap(Heap.begin(), Heap.end());
				}
			}
		}
	}
}


void GRAPH::ComputeGradient(int t)
{
	int nEdges = IndexToEdge.size();
	TotalPDForEdge.clear();
	TotalPDForEdge.resize(nEdges);

	size_t s = HeapList[t].size();
	PD.clear();
	PD.resize(s); //we could do without zero initialization here

	for(size_t c = 0; c < s; c++)
	{
		PD[c] = ImpArray[t][HeapList[t][c].i];
	}

	double tBlind = GetBlindness(t);
	for(auto it = Contrib[t].rbegin(); it != Contrib[t].rend(); it++)
	{
		auto& contrib = *it;

		int r = LabelO2N[t][contrib.first];
		int s = LabelO2N[t][contrib.second];

		int j = HeapList[t][r].i; //search source, edge dest
		int i = HeapList[t][s].i; //search dest, edge source

	//applying chain rule
		if((Q2V[i] == t) && (tBlind != 0.))
		{
			PD[r] += PD[s] * S.Prob[i][j] * tBlind;
			TotalPDForEdge[EdgeToIndex[i][j]] += PD[s] * HeapList[t][r].p * tBlind;
		}
		else
		{
			PD[r] += PD[s] * S.Prob[i][j];
			TotalPDForEdge[EdgeToIndex[i][j]] += PD[s] * HeapList[t][r].p;
		}
	}
}


void Minimize(double& a, double b)
//assigns b to a provided the new value is less
{
	if(b < a)
	{
		a = b;
	}
}


void Maximize(double& a, double b)
//assigns b to a provided the new value is greater
{
	if(b > a)
	{
		a = b;
	}
}




void
GRAPH::Init(const std::vector<int>& memory,
            const std::vector<double>& time,
            const std::vector<double>& weight,
            const std::vector<double>& blindness,
            const std::vector<EDGE_TYPE>& edge)
{
	n = ParManager.n;
	q = ParManager.q;
	g = ParManager.g;

	for(int i = 0; i < n; i++)
	{
		MemorySize[i] = memory[i];
	}

	for(int i = 0; i < g; i++)
	{
		Time[i] = time[i];
		Weight[i] = weight[i];
		Blindness[i] = blindness[i];
	}

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			VEdgeLength[i][j] = INFTY;
		}
	}

	for(auto& [from, to, length]: edge)
	{
		VEdgeLength[from][to] = length;
	}

//input done

	MaxWeight = 0.;
	for(int t = 0; t < g; t++)
	{
		if(Weight[t] > MaxWeight)
		{
			MaxWeight = Weight[t];
			tMaxWeight = t;
		}
	}

	assert(MaxWeight > 0.);

	InitVSucc(1);
	FloydWarshall(1e-6);

	for(int i = 0; i < n; i++)
	{
		if(VDist[i][i] < 0.)
		{
			throw std::runtime_error("There is a cycle of zero length");
		}
	}

	for(int t = 0; t < g; t++)
	//the targets' values are normalized
	//so that the maximum value is 1.
	{
		Weight[t] /= MaxWeight;
	}

	OrigMaxWeight = MaxWeight;
	MaxWeight = 1.;

	InitFM();
}


double SWITCHING_GRAPH::PyEvaluateSwitchingStrategy(at::Tensor /*double[Q][Q]*/ strat1, at::Tensor /*double[Q][Q]*/ strat2, double pmt)
{
	G1.LoadStrategy(strat1, 1);
	G2.LoadStrategy(strat2, 0);

	double SwitchLoss = 0.;

	for(int t = 0; t < G1.g; t++)
	{
		G2.Search(t, pmt);
		G2.ComputeSwitchingPrefixSums(t);
		G1.ComputeBeforeSwitch(G2, t, SwitchLoss, pmt);
	}

	return -SwitchLoss;
}


void GRAPH::ComputeBeforeSwitch(GRAPH& G2, int t, double& SwitchLoss, double pmt)
{
	for(int i = 0; i < q; i++)
	{
		if(!(S.IsReachable[i]))
		{
			continue;
		}

		if(Q2V[i] == t)
		{
			continue;
		}

		Heap.clear();
		HeapList[t].clear();

		Heap.push_back((HEAPITEM) {S.MaxInLength[i], 1., i, -1});

		for(int j = 0; j < q; j++)
		{
			CurListIndex[j] = std::make_pair(-1, -INFTY);
		}

		while(!(Heap.empty()))
		{
			int iList = HeapList[t].size();

			while(1)
			{
				HEAPITEM cur = Heap[0];
				std::pop_heap(Heap.begin(), Heap.end());
				Heap.pop_back();

				if(cur.d <= CurListIndex[cur.i].second + pmt)
				{
					CurListIndex[cur.i].second = cur.d;
					HeapList[t][CurListIndex[cur.i].first].d = cur.d;
					HeapList[t][CurListIndex[cur.i].first].p += cur.p;
				}
				else
				{
					CurListIndex[cur.i] ={HeapList[t].size(), cur.d};
					HeapList[t].push_back(cur);
				}

				if((Heap.empty()) || (Heap[0].d > cur.d + pmt))
				{
					break;
				}
			}

			//evaluating the loss
			double catchprob = 0.;
			double primarycatchprob = 1.;
			for(int j = iList; j < (int) HeapList[t].size(); j++)
			{
				AccountSwitchingOpportunity(HeapList[t][j], t, G2, catchprob, primarycatchprob);
			}

			for(int j = 0; j < (int) Heap.size(); j++)
			{
				AccountSwitchingOpportunity(Heap[j], t, G2, catchprob, primarycatchprob);
			}

			double prob = primarycatchprob + catchprob;
			double loss = (prob - 1.) * G2.Weight[t];
			Minimize(SwitchLoss, loss);

			for(; iList < (int) HeapList[t].size(); iList++)
			{
				HEAPITEM cur = HeapList[t][iList];
				int a = cur.i;

				for(int k = 0; k < S.OutDegree[a]; k++)
				{
					int b = S.Succ[a][k].To;
					if(Q2V[b] == t)
					{
						continue;
					}

					double d = cur.d + S.Succ[a][k].Length;

					if(d <= Time[t])
					{
						Heap.push_back((HEAPITEM) {d, cur.p * S.Prob[a][b], b, -1});
						std::push_heap(Heap.begin(), Heap.end());
					}
				}
			}
		}
	}
}


void GRAPH::AccountSwitchingOpportunity(const HEAPITEM& cur, int t, GRAPH& G2, double& catchprob, double& primarycatchprob)
{
	primarycatchprob -= cur.p;
	std::vector<PREFIXITEM>& Sum = G2.PrefixSum[cur.i];
	PREFIXITEM thres = {(Time[t] - cur.d) + 1e-6, 0.};

	auto it = std::lower_bound(Sum.begin(), Sum.end(), thres);
	double sum = (--it)->p;
	catchprob += cur.p * sum;
}


void GRAPH::ComputeSwitchingPrefixSums(int t)
{
	for(int i = 0; i < q; i++)
	{
		PrefixCount[i] = 0;
	}

	for(size_t k = 0; k < HeapList[t].size(); k++)
	{
		PrefixCount[HeapList[t][k].i]++;
	}

	for(int i = 0; i < q; i++)
	{
		PrefixSum[i].clear();
		PrefixSum[i].reserve(PrefixCount[i] + 1);
		PrefixSum[i].push_back({-1., 0.});
	}

	for(size_t k = 0; k < HeapList[t].size(); k++)
	{
		HEAPITEM cur = HeapList[t][k];
		std::vector<PREFIXITEM>& Sum = PrefixSum[cur.i];
		double sum = Sum.back().p;
		Sum.push_back({cur.d, cur.p + sum});
	}
}
