#include <torch/extension.h>
#include <ATen/ATen.h>

#include "patrolling_objective.h"

using namespace pybind11::literals;

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
    m.doc() = "Plugin for fast patrolling strategy evaluation";

    py::class_<GRAPH>(m, "CppPatrollingEvaluator")
        .def(py::init<std::vector<int>,
                std::vector<double>,
                std::vector<double>,
                std::vector<double>,
                std::vector<EDGE_TYPE>>(),
	        "Initializes the parameters",
            "memory"_a,
            "attack_len"_a,
            "value"_a,
            "blindness"_a,
            "edges"_a)
        .def("forward",
            &GRAPH::PyEvaluateStrategy,
            "Evaluates a given strategy tensor.\n"
            "Returns a tensor of shape (targets, states) of probabilities that "
            "the target t is visited from the state in more than attack_len[t].",
            "strategy"_a,
            "pmt"_a = DEFAULT_PATH_MERGE_THRESHOLD)
        .def("backward",
            &GRAPH::PyComputeGradient,
            "Given an incoming prob_grad returns the strategy_grad.",
            "prob_grad"_a);
}
