import torch
from patrolling_objective import PatrollingProbabilities


class Strategy(torch.nn.Module):
    def __init__(self, mask):
        super().__init__()
        self.mask = mask

    def forward(self, strategy_par):
        strategy = torch.exp(strategy_par) * self.mask
        strategy = torch.nn.functional.normalize(strategy, p=1, dim=1)
        return strategy


class Test(torch.nn.Module):
    def __init__(self, mask, init_data, path_merge_threshold):
        super().__init__()
        self.get_strategy = Strategy(mask)
        self.get_probabilities = PatrollingProbabilities(init_data, path_merge_threshold)

    def forward(self, strategy_par):
        strategy = self.get_strategy(strategy_par)
        return self.get_probabilities(strategy)


def test():
    data = dict(memory=[1, 1, 2],
                attack_len=[1.0, 2.0],
                value=[3.0, 4.0],
                blindness=[0.0, 0.0],
                edges=[(0, 1, 1.0),
                       (0, 2, 1.0),
                       (1, 2, 1.0),
                       (1, 0, 1.0),
                       (2, 0, 1.0),
                       (2, 1, 1.0)])

    get_probabilities = PatrollingProbabilities(data, path_merge_threshold=0)

    mask = torch.tensor([[0, 1, 1, 1],
                         [1, 0, 1, 1],
                         [1, 1, 0, 0],
                         [1, 1, 0, 0]], dtype=torch.bool)

    get_strategy = Strategy(mask)

    params = torch.tensor([[0.0000, 0.3027, 0.1624, 0.5349],
                           [0.4412, 0.0000, 0.0500, 0.5088],
                           [0.4113, 0.5887, 0.0000, 0.0000],
                           [0.3173, 0.6827, 0.0000, 0.0000]], dtype=torch.float64, requires_grad=True)

    strategy_par = torch.nn.Parameter(torch.log(params))
    # strategy_par = torch.nn.Parameter(torch.log(torch.rand_like(mask, dtype=torch.float, requires_grad=True)))

    strategy = get_strategy(strategy_par)
    print(f'{strategy=}')

    probabilities = get_probabilities(strategy)
    print(f'{probabilities=}')

    steals = torch.tensor(data['value'])[:, None] * probabilities
    print(f'{steals=}')

    value = max(data['value']) - torch.max(steals).item()
    print(f'{value=}')

    loss = torch.sum(torch.abs(torch.flatten(steals) ** 2))
    loss.backward()

    print(f'{strategy_par.grad=}')

    # Gradient Check
    torch.autograd.gradcheck(Test(mask, data, 0),
                             [torch.log(params).double()],
                             atol=1e-5, rtol=0, raise_exception=True)


if __name__ == '__main__':
    test()
