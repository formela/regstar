from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension


setup(name='patrolling',
      ext_modules=[
            CppExtension(
                  'patrolling',
                  ['patrolling/main.cpp'],
                  extra_compile_args=['-std=c++17', '-D_SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING=1']
            ),
      ],
      cmdclass={'build_ext': BuildExtension},
      # version="0.2.1",
      # author="David Klaška, Vít Musil",
      # author_email="musil@fi.muni.cz",
      # url="https://gitlab.fi.muni.cz/musil/PSEM",
      description="Patrolling strategy evaluator")
