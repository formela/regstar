import torch

# # Uncomment for JIT compilation
# from torch.utils.cpp_extension import load
#
# patrolling = load(name="patrolling",
#                   sources=["patrolling/main.cpp"],
#                   verbose=True)

from patrolling import CppPatrollingEvaluator


class PatrollingProbabilities(torch.nn.Module):
    def __init__(self, init_data, path_merge_threshold):
        super().__init__()
        self.path_merge_threshold = path_merge_threshold
        self.evaluator = CppPatrollingEvaluator(**init_data)

    def forward(self, strategy):
        # no kwargs in apply
        # https://github.com/pytorch/pytorch/issues/16940
        return EvaluatorWrapper.apply(strategy, self.evaluator, self.path_merge_threshold)


class EvaluatorWrapper(torch.autograd.Function):
    @staticmethod
    def forward(ctx, strategy, evaluator, *args):
        prob = evaluator.forward(strategy.detach(), *args)
        ctx.evaluator = evaluator
        return prob

    @staticmethod
    def backward(ctx, prob_grad):
        strategy_grad = ctx.evaluator.backward(prob_grad)
        return strategy_grad, None, None
