from copy import deepcopy

import networkx
from geopy.distance import distance as geopy_distance

from itertools import product, combinations
import random
import os
import math

"""
    Add your favourite graph here!
    -> don't forget to add it to 'graphs_dict' to make it available in settings
"""


def hardcoded(nodes, targets, edges, settings=None):
    nodes = nodes or {}
    targets = targets or {}
    edges = edges or {}
    settings = settings or {}
    node_att = settings.get('node_att', {}).copy()
    node_att.update(target=False)
    target_att = settings.get('target_att', {}).copy()
    target_att.update(target=True)
    edge_att = settings.get('edge_att', {}).copy()

    nodes = [(node, {**node_att, **(att or {})}) for node, att in nodes.items()]
    targets = [(target, {**target_att, **(att or {})}) for target, att in targets.items()]
    edges = [(*edge.get('nodes'), {**edge_att, **edge}) for edge in edges]

    directed = settings.get('directed', True)
    graph = networkx.DiGraph() if directed else networkx.Graph()
    graph.add_nodes_from(targets + nodes)
    graph.add_edges_from(edges)
    graph.name = 'hardcoded'

    return graph


def complete_graph(num_nodes, node_att=None):
    node_attributes = dict(value=100, attack_len=num_nodes, blindness=0.0, memory=1, target=True)
    node_attributes.update(node_att or {})
    edge_attributes = dict(len=1)
    graph = networkx.Graph()
    nodes = list(range(num_nodes))
    edges = combinations(nodes, 2)
    graph.add_nodes_from(nodes, **node_attributes)
    graph.add_edges_from(edges, **edge_attributes)
    graph.name = f'{num_nodes}_clique'
    return graph


def office_building(floors, halls, office_att=None, hall_att=None):
    office_attributes = dict(value=100, attack_len=112, blindness=0.0, memory=1, target=True)
    office_attributes.update(office_att or {})
    hall_attributes = dict(memory=4, target=False)
    hall_attributes.update(hall_att or {})
    graph = networkx.Graph()
    nodes = []
    edges = []
    for floor in range(floors):
        for hall in range(halls):
            up = f'Office_{floor}_{hall}up'
            dn = f'Office_{floor}_{hall}dn'
            cr = f'Hall_{floor}_{hall}'
            nodes += [(up, office_attributes),
                      (dn, office_attributes),
                      (cr, hall_attributes)]
            edges += [(cr, up, dict(len=5)),
                      (cr, dn, dict(len=5))]

        le = f'Office_{floor}_0le'
        ri = f'Office_{floor}_{halls - 1}ri'
        nodes += [(le, office_attributes),
                  (ri, office_attributes)]
        edges += [(le, f'Hall_{floor}_0', dict(len=5)),
                  (ri, f'Hall_{floor}_{halls - 1}', dict(len=5))]

        for hall in range(halls - 1):
            edges.append((f'Hall_{floor}_{hall}', f'Hall_{floor}_{hall + 1}', dict(len=2)))

    for floor in range(floors - 1):
        edges += [(f'Hall_{floor}_0', f'Hall_{floor + 1}_0', dict(len=10)),
                  (f'Hall_{floor}_{halls - 1}', f'Hall_{floor + 1}_{halls - 1}', dict(len=10))]

    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    graph.name = f'office_building_{floors}_f_{halls}_h'

    return graph


def distance(from_vert, to_vert, taxicab=True, distance_unit=None) -> float:
    """Computes distance between two points.

    Attributes:
        from_vert      pair of the source coordinates
        to_vert        pair of the target coordinates
        taxicab        usage of taxicab or Euclidean metric
        distance_unit  if not None, used as a parameter for convergence from GPS coordinates
                       (e.g., 'm' or 'meters', 'mi' or 'miles', 'km' or 'kilometers')
    """
    if distance_unit is None:
        # no GPS, no unit
        if taxicab:
            return abs(from_vert[0] - to_vert[0]) + abs(from_vert[1] - to_vert[1])
        else:
            return math.sqrt((from_vert[0] - to_vert[0]) ** 2 + (from_vert[1] - to_vert[1]) ** 2)

    # GPS, units by ‹distance_unit›

    if taxicab:
        d = geopy_distance(from_vert, (to_vert[0], from_vert[1])) + \
            geopy_distance(from_vert, (from_vert[0], to_vert[1]))
    else:
        # Euclidean distance
        d = geopy_distance(from_vert, to_vert)

    # convert to units given by ‹distance_unit›
    return getattr(d, distance_unit)


def square_subgraph_one_param(num_nodes, node_seed=13, node_att=None):
    num_targets = num_nodes // 2
    return square_subgraph(num_nodes, num_nodes,  num_targets, node_att, node_seed)


def square_subgraph(side_length, num_nodes, num_targets=None, node_att=None, node_seed=13, param_seed=13, taxicab=True,
                    blind=False):
    """Generates random graphs based on grids.
    The graph nodes are randomly chosen points of grid of size ‹side_length› ⨉ ‹side_length›.
    ‹num_nodes› nodes are chosen. The graph edges are computed in the taxicab distance metric by
    default. Euclidean metric is set by ‹taxicab=False›.

    Attributes:
        side_length    side length of the original grid
        num_nodes      number of the chosen nodes
        num_targets    number of targets ( <= num_nodes; if None, it equals to num_nodes)
        node_att       optional attributes updating the default values
        node_seed      the seed for the random generator - for nodes
        param_seed     the seed for the random generator - for parameters
        taxicab        usage of taxicab or Euclidean metric
        blind          True for random blindness in nodes
        verbose        True for printing the generated graph

    Backward compatibility:
       Sub_g_6_n_01.in is generated by square_subgraph(6, 10, node_seed=1)
    """
    # safety check
    if not (1 <= num_nodes <= side_length * side_length):
        raise AttributeError(f"Error: wrong value for parameter num_nodes, "
                             f"not (1 <= {num_nodes} <= {side_length * side_length}).")

    if num_targets is None:
        num_targets = num_nodes

    if num_nodes < num_targets:
        raise AttributeError(f"Error: wrong value for parameter num_targets, not ({num_targets} <= {num_nodes}).")

    # choose the nodes
    pool = [edge for edge in product(range(side_length), repeat=2)]
    random.seed(node_seed)
    random.shuffle(pool)
    chosen_nodes = pool[:num_nodes]
    chosen_targets = chosen_nodes[:num_targets]
    chosen_nodes.sort()

    # compute distances/edges
    edges_dist = [distance(*edge, taxicab=taxicab) for edge in combinations(chosen_nodes, 2)]

    # edge-length statistics
    max_edge = max(edges_dist)
    mean_edge = sum(edges_dist) / len(edges_dist)
    attack_time = int(max_edge + mean_edge + 3) if taxicab else int(2 * max_edge + mean_edge)

    random.seed(param_seed)

    nodes = []
    for node in chosen_nodes:
        node_attributes = dict(value=random.randint(180, 200),
                               attack_len=attack_time,
                               blindness=random.randint(0, 20) / 100 if blind else 0,
                               memory=4,
                               target=(node in chosen_targets))
        node_attributes.update(node_att or {})
        nodes.append((node, node_attributes))

    edges = []
    for edge, dist in zip(combinations(chosen_nodes, 2), edges_dist):
        edges.append((*edge, dict(len=dist)))

    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    graph = graph.to_directed()
    graph.name = f"square_subgraph_s{side_length}_n{num_nodes}_t{num_targets}"

    return graph


def motreal_backcomp(kml_file_name, node_att=None, distance_unit='meters', rounding=100, seed=13):
    from xml.dom import minidom
    my_doc = minidom.parse(os.path.join(os.getcwd(), kml_file_name))
    coordinates = my_doc.getElementsByTagName('coordinates')
    names = my_doc.getElementsByTagName('name')[1:]
    num_nodes = len(coordinates)
    chosen_nodes = []

    for elem in coordinates:
        (y, x, _) = elem.firstChild.data.strip().split(',')
        chosen_nodes.append((float(y), float(x)))

    edges_dist = [math.ceil(distance(from_vert, to_vert, True, distance_unit)/rounding)
                  for from_vert, to_vert in product(chosen_nodes, chosen_nodes)]

    max_edge = max(edges_dist)
    mean_edge = sum(edges_dist) / (num_nodes * (num_nodes - 1))
    attack_time = int(2 * max_edge + mean_edge)

    if seed:
        random.seed(seed)
    costs = [random.randint(180, 200) for _ in range(num_nodes)]
    blinds = [random.randint(0, 20) / 100 for _ in range(num_nodes)]

    nodes = []
    edges = []
    for i in range(num_nodes):
        from_name = f'{i}_{names[i].firstChild.data}'
        node_attributes = dict(value=costs[i], attack_len=attack_time, blindness=blinds[i], memory=4, target=True)
        node_attributes.update(node_att or {})
        nodes.append((from_name, node_attributes))
        for j in range(i + 1, num_nodes):
            edges.append((from_name, f'{j}_{names[j].firstChild.data}', dict(len=round(edges_dist[i * num_nodes + j]))))

    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)

    return graph


def kml_graph(kml_file_name, node_att=None, taxicab=False, distance_unit='meters', rounding=1, seed=None):
    """Generates graphs from a kml file - exported from Google maps.
    Attributes:
        kml_file_name  path to the kml file
        node_att       optional attributes updating the default values
        seed           the seed for the random generator
        taxicab        usage of taxicab or Euclidean metric
        distance_unit  if not None, used as a parameter for convergence from GPS coordinates
                       ('meters', 'miles')
        rounding       distances rounding
    """
    if seed is not None:
        random.seed(seed)

    from xml.dom import minidom
    kml_file = minidom.parse(kml_file_name)
    # get names (the fist one is the Document name)
    names = kml_file.getElementsByTagName('name')[1:]
    names = [f'{i}_{name.firstChild.data}' for i, name in enumerate(names)]
    # get coordinates
    coordinates = kml_file.getElementsByTagName('coordinates')
    chosen_nodes = []
    for elem in coordinates:
        (x, y, _) = elem.firstChild.data.strip().split(',')
        # x and y are switched correctly, see documentation
        chosen_nodes.append((float(y), float(x)))

    edges_dist = [math.ceil(distance(from_node, to_node, taxicab, distance_unit)/rounding)
                  for from_node, to_node in combinations(chosen_nodes, 2)]

    attack_time = int(2 * max(edges_dist) + sum(edges_dist) / len(edges_dist))

    nodes = []
    for name in names:
        node_attributes = dict(value=random.randint(90, 100),
                               attack_len=attack_time,
                               blindness=0.0, memory=4, target=True)
        node_attributes.update(node_att or {})
        nodes.append((name, node_attributes))

    edges = []
    for (from_name, to_name), dist in zip(combinations(names, 2), edges_dist):
        edge_attributes = dict(len=round(dist))
        edges.append((from_name, to_name, edge_attributes))

    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)

    return graph


def airport(hall_distances, gates, default_gate_att, default_hall_att):
    nodes = [('center', default_hall_att)]
    edges = []
    # building halls
    for leg_index, leg in enumerate(hall_distances):
        previous = nodes[0][0]
        for hall_index, hall_distance in enumerate(leg):
            new_node = f"H_{leg_index}_{hall_index}"
            nodes.append((new_node, default_hall_att))
            edges.append((previous, new_node, {'len': hall_distance}))
            previous = new_node
    # building gates
    for leg_index, leg in enumerate(gates):
        for hall_index, hall_gates in enumerate(leg):
            hall_name = f"H_{leg_index}_{hall_index}"
            for gate in hall_gates:
                g_name = gate.pop('name')
                g_dist = gate.pop('dist')
                g_att = default_gate_att.copy()
                g_att.update(gate)
                nodes.append((g_name, g_att))
                edges.append((hall_name, g_name, {'len': g_dist}))
    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    return graph


def airport_generator(halls_in_legs, default_gate_att, default_hall_att, gates=2, rnd_values=False, seed=None):
    if seed is not None:
        random.seed(seed)
    nodes = [('center', default_hall_att)]
    edges = []
    for leg_index in range(len(halls_in_legs)):
        previous = nodes[0][0]
        for hall_index in range(halls_in_legs[leg_index]):
            # hall
            hall_name = f"H_{leg_index}_{hall_index}"
            nodes.append((hall_name, default_hall_att))
            edges.append((previous, hall_name, {'len': 1}))
            previous = hall_name
            # gates
            for gate_index in range(gates):
                if rnd_values:
                    default_gate_att.update(value=random.randint(5, 15)/10)
                g_name = f"G_{leg_index}_{hall_index}_{gate_index}"
                nodes.append((g_name, default_gate_att))
                edges.append((hall_name, g_name, {'len': 1}))
    graph = networkx.Graph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    return graph


def airport_one_param(num_halls, rnd_seed=0, rnd_values=False):
    # 3-terminal airport with 'num_halls' halls, at least one in each terminal
    if num_halls<3:
        raise AttributeError(f"Error: wrong value for parameter num_halls = {num_halls}, not >=3.")
    num_halls-=3  # one default edge for each terminal
    if rnd_seed is not None:
        random.seed(num_halls * rnd_seed)
    hall_sep = [random.randint(0, num_halls), random.randint(0, num_halls)]
    hall_sep.sort()
    halls_in_legs = [hall_sep[0], hall_sep[1]-hall_sep[0], num_halls - hall_sep[1]]
    halls_in_legs = [x+1 for x in halls_in_legs]  # returns the default edges back
    gate_att = dict(target=True, value=1, attack_len=1, blindness=0.0, memory=1)
    hall_att = dict(target=False, memory=4)
    return airport_generator(halls_in_legs, gate_att, hall_att, gates=2, seed=rnd_seed, rnd_values=rnd_values)


def diamond_ring(values, node_att=None, target_att=None, edge_att=None):
    node_attribute = dict(target=False, memory=max(map(len, values)))
    node_attribute.update(node_att or {})
    target_attribute = dict(target=True, value=1, memory=1, attack_len=1, blindness=0.0)
    target_attribute.update(target_att or {})
    edge_attribute = dict(len=1)
    edge_attribute.update(edge_att or {})

    nodes = [('start', node_attribute)]
    edges = []
    start_node = 'start'
    for d_idx, diamond in enumerate(values):
        if d_idx == len(values)-1:
            end_node = nodes[0][0]
        else:
            end_node = f"d_{d_idx}"
            nodes.append((end_node, node_attribute))
        for v_idx, val in enumerate(diamond):
            n_att = target_attribute.copy()
            n_att['value'] = val
            name = f"d_{d_idx}_n_{v_idx}_v_{val}"
            nodes.append((name, n_att))
            edges.append((start_node, name, edge_attribute))
            edges.append((name, end_node, edge_attribute))
        start_node = end_node
    graph = networkx.DiGraph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    return graph


def diamond(values, node_att=None, target_att=None, edge_att=None):
    node_attribute = dict(target=False, memory=len(values))
    node_attribute.update(node_att or {})
    target_attribute = dict(target=True, value=1, memory=1, attack_len=1, blindness=0.0)
    target_attribute.update(target_att or {})
    edge_attribute = dict(len=1)
    edge_attribute.update(edge_att or {})

    nodes = [('start', node_attribute), ('stop', node_attribute)]
    edges = [('stop', 'start', edge_attribute)]
    for idx, val in enumerate(values):
        n_att = target_attribute.copy()
        n_att['value'] = val
        name = f"n_{idx}_v_{val}"
        nodes.append((name, n_att))
        edges.append(('start', name, edge_attribute))
        edges.append((name, 'stop', edge_attribute))
    graph = networkx.DiGraph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)
    return graph


graphs_dict = {
    'hardcoded': hardcoded,
    'office_building': office_building,
    'square_subgraph': square_subgraph,
    'square_subgraph_one_param': square_subgraph_one_param,
    'kml_graph': kml_graph,
    'complete_graph': complete_graph,
    'motreal_backcomp': motreal_backcomp,
    'airport': airport,
    'airport_generator': airport_generator,
    'airport_one_param': airport_one_param,
    'diamond': diamond,
    'diamond_ring': diamond_ring
}
