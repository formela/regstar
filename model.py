import numpy as np
import torch
from termcolor import cprint
from cpp_extensions.patrolling_objective import PatrollingProbabilities


class Strategy(torch.nn.Module):
    def __init__(self, mask, rounding_threshold, checkpoint=None):
        super().__init__()
        self.mask = mask
        self.threshold = rounding_threshold
        self.checkpoint = checkpoint
        self.params = torch.nn.Parameter(self._init_params())
        if self.checkpoint:
            # consider checkpointing the final Game module
            self.load_state_dict(state_dict=torch.load(self.checkpoint))

    def _init_params(self):
        return self.mask * torch.log(torch.rand_like(self.mask, dtype=torch.float64))

    def reset_parameters(self):
        self.params.data = self._init_params()

    def forward(self):
        x = torch.exp(self.params) * self.mask
        strategy = torch.nn.functional.normalize(x, p=1, dim=1)
        if not self.training:
            x = torch.threshold(strategy, threshold=self.threshold, value=0.0)
            strategy = torch.nn.functional.normalize(x, p=1, dim=1)
        return strategy


class Game(torch.nn.Module):
    def __init__(self, strategy, objective=None):
        super().__init__()
        self.strategy = strategy
        self.objective = objective

    def forward(self):
        strategy = self.strategy()
        return self.objective(strategy)


class GenericObjective(object):
    def __init__(self, graph, relaxed_max=None, relaxed_max_params=None, verbose=False):
        self.graph = graph
        self.mask = graph.mask
        self.times = graph.times
        self.longest = graph.longest
        self.costs = graph.costs
        self.target_mask = graph.target_mask
        self.aug_nodes_n = graph.aug_nodes_n

        self.last_expectations = 0
        self.last_second_moment = 0
        self.last_variance = 0
        self.last_node_stationary = 0

        self.verbose = verbose
        if relaxed_max:
            relaxed_max_params = relaxed_max_params or {}
            self.relaxed_max = loss_from_string(relaxed_max)(**relaxed_max_params)

    def early_stop(self, val):
        return False

    def get_expectations(self, strategy):
        A = torch.eye(self.aug_nodes_n) - strategy * self.target_mask[:, :, None]
        b = (strategy * self.times).sum(dim=1) * self.target_mask
        self.last_expectations = solve_system(A, b, self.last_expectations, self.verbose)
        return self.last_expectations

    def get_second_moment(self, strategy, expectations):
        A = torch.eye(self.aug_nodes_n) - strategy * self.target_mask[:, :, None]
        b = (strategy * self.times * (self.times + 2 * expectations[:, None])).sum(dim=2) * self.target_mask
        self.last_second_moment = solve_system(A, b, self.last_second_moment, self.verbose)
        return self.last_second_moment

    def get_variance(self, strategy, expectations):
        A = torch.eye(self.aug_nodes_n) - strategy * self.target_mask[:, :, None]
        b = (strategy * (self.times + expectations[:, None, :] - expectations[:, :, None]) ** 2).sum(
            dim=2) * self.target_mask
        self.last_variance = solve_system(A, b, self.last_variance, self.verbose)
        return self.last_variance

    def get_std(self, strategy, expectations):
        return torch.sqrt(self.get_variance(strategy, expectations) + 1e-14)

    def get_node_stationary(self, strategy):
        B = strategy.transpose(0, 1) - torch.eye(self.aug_nodes_n)
        A = torch.cat((B[:-1], torch.ones_like(B[0])[None, :]), 0)
        b = torch.zeros_like(strategy[0])
        b[-1] = 1
        self.last_node_stationary = solve_system(A, b, self.last_node_stationary, self.verbose)
        return self.last_node_stationary

    def get_edge_stationary(self, strategy):
        node_stationary = self.get_node_stationary(strategy)
        distribution = strategy * node_stationary[:, None] * self.times
        return distribution / distribution.sum()

    def get_p_values(self, strategy, levels):
        p_values = []
        if levels:
            # tensor - for each target, strategy matrix with zero rows for the target
            x = strategy * self.target_mask[:, :, None]
            # tensor - for each target, strategy matrix with self-loops on the target nodes
            x = x + torch.eye(self.aug_nodes_n) * ~self.target_mask[:, None]
            for level in levels:
                # steal -> steps, "-1" stands for the first edge (on which the attack starts)
                powers = torch.floor(level / self.costs) - 1
                y = x.clone().detach()
                for idx, power in enumerate(powers):
                    y[idx] = torch.matrix_power(y[idx], int(power))
                # sum probs for different mem. elems of each target, select the best attack
                N = (y * ~self.target_mask[:, None]).max(dim=2).values
                # in N, there are probs to reach target within the bound; i.e. P(steal <= level)
                # so, we return complementary P(steal > bound) = 1 - N
                p_values.append((1 - N).max().detach().item())
        return p_values


class MeanPayoff(GenericObjective, torch.nn.Module):
    mode = 'min'

    def __init__(self, graph, beta=1, verbose=False):
        torch.nn.Module.__init__(self)
        GenericObjective.__init__(self, graph=graph, verbose=verbose)
        self.beta = beta

    def forward(self, strategy):
        target_stationary = self.get_node_stationary(strategy) * ~self.target_mask
        target_stationary = target_stationary.sum(dim=1)
        payoff_e = (target_stationary * self.costs).sum()
        payoff_var = (target_stationary * (payoff_e - self.costs) ** 2).sum()
        payoff_std = torch.sqrt(payoff_var + 1e-14)

        val = payoff_e + self.beta * payoff_std
        if self.verbose:
            np.set_printoptions(linewidth=200, precision=4, suppress=True)
            print("\npayoff_e:", payoff_e.numpy())
            print("\npayoff_var:", payoff_var.numpy())
            print("\npayoff_std:", payoff_std.numpy())
            print("\nVal:", val.numpy())
            print("\ntarget_stationary:", target_stationary.numpy())

        extras = {'cost_e': payoff_e.item(),
                  'cost_std': payoff_std.item()}
        if self.training:
            return val, val.item(), extras
        else:
            return val.item(), strategy.numpy(), extras


class RenewalTime(GenericObjective, torch.nn.Module):
    mode = 'min'

    def __init__(self, graph, beta=0.1, relaxed_max=None, relaxed_max_params=None, verbose=False):
        torch.nn.Module.__init__(self)
        GenericObjective.__init__(self, graph=graph, relaxed_max=relaxed_max, relaxed_max_params=relaxed_max_params,
                                  verbose=verbose)
        self.beta = beta

    def forward(self, strategy):
        target_stationary = self.get_node_stationary(strategy) * ~self.target_mask
        target_stationary = target_stationary / target_stationary.sum(dim=1)[:, None]

        time_e = self.get_expectations(strategy)
        target_time_e = ((self.times + time_e[:, None]) * strategy).sum(dim=2) * ~self.target_mask
        renewal_time_e = (target_time_e * target_stationary).sum(dim=1)
        renewal_cost_e = self.costs[:, None] * renewal_time_e

        time_2m = self.get_second_moment(strategy, time_e)
        target_time_2m = ((self.times * (self.times + 2 * time_e[:, None]) + time_2m[:, None]) * strategy).sum(
            dim=2) * ~self.target_mask
        renewal_time_2m = (target_time_2m * target_stationary).sum(dim=1)

        renewal_time_std = torch.sqrt(renewal_time_2m - renewal_time_e ** 2 + 1e-14)
        renewal_cost_std = self.costs[:, None] * renewal_time_std

        val = renewal_cost_e + self.beta * renewal_cost_std
        val_max, val_arg_max = val.flatten().max(dim=0)

        if self.verbose:
            np.set_printoptions(linewidth=200, precision=4, suppress=True)
            print("\nCost_e:\n", renewal_cost_e.detach().numpy())
            print("\nCost_std:\n", renewal_cost_std.detach().numpy())
            print("\nVal:\n", val.detach().numpy())
            print("\nVal_max:", val_max.item())
            print("\npos_of_max:", val_arg_max.item(), "\n\n")

        extras = {'cost_e': renewal_cost_e.flatten()[val_arg_max].item(),
                  'cost_std': renewal_cost_std.flatten()[val_arg_max].item()}

        if self.training:
            loss = self.relaxed_max(val)
            return loss, val_max.item(), extras
        else:
            return val_max.item(), strategy.numpy(), extras


class AdversarialPatrolling(GenericObjective, torch.nn.Module):
    # todo: Think about the name, Linear damage patrolling?
    mode = 'min'

    def __init__(self, graph, beta=1, cutoff_threshold=0.1, relaxed_max=None, relaxed_max_params=None, verbose=False):
        torch.nn.Module.__init__(self)
        GenericObjective.__init__(self, graph=graph, relaxed_max=relaxed_max, relaxed_max_params=relaxed_max_params,
                                  verbose=verbose)
        self.beta = beta
        self.cutoff_threshold = cutoff_threshold
        # todo: rename

    def forward(self, strategy):
        time_e = self.get_expectations(strategy)
        time_std = self.get_std(strategy, time_e)
        cost_std = self.costs[:, None] * time_std
        mask_used = (strategy > 0) if self.training else torch.nn.functional.hardtanh(strategy * self.cutoff_threshold)
        time_e = (self.times * mask_used).max(dim=0).values + time_e
        cost_e = self.costs[:, None] * time_e

        val = cost_e + self.beta * cost_std
        val_max, val_arg_max = val.flatten().max(dim=0)

        if self.verbose:
            np.set_printoptions(linewidth=200, precision=4, suppress=True)
            print("\nCost_e:\n", cost_e.numpy())
            print("\nCost_std:\n", cost_std.numpy())
            print("\nVal:\n", val.numpy())
            print("\nVal_max:", val_max.item())
            print("\npos_of_max:", val_arg_max.item(), "\n\n")

        extras = {'cost_e': cost_e.flatten()[val_arg_max].item(),
                  'cost_std': cost_std.flatten()[val_arg_max].item()}
        if self.training:
            loss = self.relaxed_max(val)
            return loss, val_max.item(), extras
        else:
            return val_max.item(), strategy.numpy(), extras


def mean_entropy(strategy):
    return - torch.sum(strategy * torch.log(strategy + (strategy == 0)), dim=1).mean()


class AdversarialPatrollingWithEntropy(AdversarialPatrolling, torch.nn.Module):

    def forward(self, strategy):
        mask_used = (strategy > 0) if self.training else torch.nn.functional.hardtanh(strategy * self.cutoff_threshold)
        time_e = self.get_expectations(strategy) + (self.times * mask_used).max(dim=0).values
        cost_e = self.costs[:, None] * time_e
        entropy = mean_entropy(strategy)

        val = cost_e
        val_max, val_arg_max = val.flatten().max(dim=0)
        if self.verbose:
            np.set_printoptions(linewidth=200, precision=4, suppress=True)
            print("\nEntropy:", entropy.item())
            print("\nVal:\n", val.numpy())
            print("\nVal_max:", val_max.item())
            print("\npos_of_max:", val_arg_max.item(), "\n\n")

        if self.training:
            raw_loss = self.relaxed_max(val)
            loss = raw_loss + self.beta * raw_loss.detach() * entropy
            extras = {'raw_loss': raw_loss.item(),
                      'train_entropy': entropy.item()}
            return loss, val_max.item(), extras
        else:
            extras = {'entropy': entropy.item()}
            return val_max.item(), strategy.numpy(), extras


class Patrolling(torch.nn.Module):
    # todo: think about the name
    mode = 'max'

    def __init__(self, graph,
                 path_merge_threshold=0,
                 relaxed_max=None, relaxed_max_params=None,
                 verbose=False):
        super().__init__()

        self.graph = graph
        self.costs = graph.costs
        self.costs_max = max(self.costs).item()
        self.verbose = verbose

        relaxed_max_params = relaxed_max_params or {}
        if relaxed_max:
            self.relaxed_max = loss_from_string(relaxed_max)(**relaxed_max_params)

        self.attack_prob = PatrollingProbabilities(graph.get_patrolling_init_data(), path_merge_threshold)

    def early_stop(self, val):
        return val == self.costs_max

    def forward(self, strategy):
        steals = self.costs[:, None] * self.attack_prob(strategy)
        val = self.costs_max - torch.max(steals).item()
        if self.training:
            loss = self.relaxed_max(steals)
            return loss, val, None
        else:
            return val, strategy.numpy(), None


def print_grad(steal_grads):
    cprint('steal_grads:', 'blue')
    with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
        for grad in steal_grads:
            print(' ', np.array2string(grad.numpy(), prefix='  '), '\n')


def solve_system(A, b, last_solution, verbose=False):
    try:
        solution = torch.linalg.solve(A, b)
    except RuntimeError as error_msg:
        cprint('torch.linalg.solve failed with runtime error:', 'red')
        print(error_msg)
        if verbose:
            cprint('A:', 'blue')
            print(A)
            cprint('b:', 'blue')
            print(b)
        solution = last_solution
        cprint(f'continuing with the last result...', 'green')
    return solution


class HardMaxLoss(torch.nn.Module):
    def forward(self, steal):
        max_steal = torch.max(steal)
        return max_steal


class PNormClampLoss(torch.nn.Module):
    def __init__(self, eps, ord):
        super().__init__()
        self.eps = eps
        self.ord = ord

    def forward(self, steal):
        max_steal = torch.max(steal)
        max_steal_static = max_steal.detach()
        eps = self.eps * max_steal_static
        steal = (steal - max_steal_static + eps) / eps
        steal = torch.clamp(steal, min=0.0)
        loss = torch.sum(steal ** self.ord)
        return loss


class PNormLoss(torch.nn.Module):
    def __init__(self, ord):
        super().__init__()
        self.ord = ord

    def forward(self, steal):
        steal = torch.flatten(steal)
        loss = torch.sum(torch.abs(steal ** self.ord))
        return loss


class LogSumExpClampLoss(torch.nn.Module):
    def __init__(self, eps, tau):
        super().__init__()
        self.eps = eps
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        max_steal = torch.max(steal).detach()
        eps = self.eps * max_steal
        steal = torch.threshold(steal, threshold=max_steal - eps, value=0.0)
        steal = steal[steal.nonzero(as_tuple=True)]
        loss = torch.logsumexp(steal * self.tau, dim=0) / self.tau
        return loss


class LogSumExpLoss(torch.nn.Module):
    def __init__(self, tau):
        super().__init__()
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        loss = torch.logsumexp(steal * self.tau, dim=0) / self.tau
        return loss


class SoftMaxLoss(torch.nn.Module):
    def __init__(self, tau):
        super().__init__()
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        softmax_steal = torch.softmax(steal * self.tau, dim=0)
        loss = torch.sum(steal * softmax_steal, dim=0)
        return loss


objectives_dict = {'Patrolling': Patrolling,
                   'AdversarialPatrolling': AdversarialPatrolling,
                   'RenewalTime': RenewalTime,
                   'MeanPayoff': MeanPayoff,
                   'Fire': AdversarialPatrolling,
                   'AdversarialPatrollingWithEntropy': AdversarialPatrollingWithEntropy}  # backward compatibility


def game_from_string(game_name):
    return objectives_dict[game_name]


loss_dict = {'HardMax': HardMaxLoss,
             'PNorm': PNormLoss,
             'PNormClamp': PNormClampLoss,
             'LogSumExp': LogSumExpLoss,
             'LogSumExpClamp': LogSumExpClampLoss,
             'SoftMax': SoftMaxLoss}


def loss_from_string(loss):
    return loss_dict[loss]
