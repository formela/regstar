# Patrolling Strategy Optimization

## Preparation

Create virtual environment and install all packages:

1. Make sure you have `python 3` and `pipenv` installed
2. Run `python3 -m pipenv install` (at your own risk with `--skip-lock`)
3. Run the environment `python3 -m pipenv shell`
4. Install ccp_extensions by `cd cpp_extensions; python setup.py build install; cd ..`


## Optimization

Run the optimization (in the above installed pipenv shell) by
```shell
python3 train.py path/to/your/settings.yml
```

where `path/to/your/settings.yml` is the path to your configuration file.
For more details, see `setting_template.yml` or the examples in `experiments/.../*.yml`.


## Results

The results, visualizations and logs are stored in a directory specified in
`reporting_params.database_params.results_dir`. Usually, it is
`results/<experiment_name>/<date>/<time>`.
The directories with results include:

* `stats_epoch.csv` Detailed statistics for all epochs
* `stats_final.csv` Statistics summary over all epochs
* `stats_full.csv` Detailed statistics for all steps of all epochs
* `times_full.csv` Time statistics for all steps of all epochs

and optionally also:

* `check_points` Directory with exports of all torch parameter values
* `strategies` Directory with optimized strategies exported as numpy matrices
* `plots` Directory with plots:
  * `avg_times.pdf` Plot of average runtimes ("forward", "backward", and "other" times)
  * `training_progress.pdf` Plots of strategy values during optimization
