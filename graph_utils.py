from collections import Counter
import torch
import networkx
import numpy as np
from itertools import product
from termcolor import cprint
from graph_collections import graphs_dict
from graph_modifiers import graph_modifier_from_string


class Graph(object):
    def __init__(self, loader, loader_params, modifiers=None, verbose=False, name=None):
        self.verbose = verbose
        self.name = name
        self.graph = load_graph_with_modifiers(loader, loader_params, modifiers, verbose)
        self.graph = self.graph.to_directed()

        self.targets, self.non_targets = [], []
        for (node, is_target) in self.graph.nodes.data('target'):
            (self.non_targets, self.targets)[is_target].append(node)
        self.targets_n = len(self.targets)

        self.nodes = self.targets + self.non_targets  # external evaluators require targets first
        self.nodes_n = len(self.nodes)

        # collecting node features
        target_features = ['value', 'memory', 'attack_len', 'blindness']
        self.target_features = get_node_features(self.graph, self.targets, target_features)
        non_target_features = ['memory']
        self.non_target_features = get_node_features(self.graph, self.non_targets, non_target_features)

        # integer encoding for external evaluators
        self.node_map = {node: index for index, node in enumerate(self.nodes)}
        self.edges = [(self.node_map[a], self.node_map[b], att['len']) for a, b, att in self.graph.edges.data()]

        # graph augmentation with memory elements
        self.aug_nodes, aug_nodes_att = [], []
        for node in self.nodes:
            self.aug_nodes += [(node, mem) for mem in range(self.graph.nodes[node]['memory'])]
            # aug_nodes_att += [self.graph.nodes[node]] * self.graph.nodes[node]['memory']
        self.aug_node_map = {node: index for index, node in enumerate(self.aug_nodes)}
        self.aug_nodes_n = len(self.aug_nodes)

        self.aug_graph = networkx.DiGraph()
        self.aug_graph.add_nodes_from(self.aug_nodes)  # the nodes do not contain their attributes

        for source, target, att in self.graph.edges.data():
            source_memory = self.graph.nodes[source]['memory']
            target_memory = self.graph.nodes[target]['memory']
            mem_product = product(range(source_memory), range(target_memory))
            aug_edges = [((source, ms), (target, mt), att) for ms, mt in mem_product]
            self.aug_graph.add_edges_from(aug_edges)

        # edge_len matrix indexed by aug_nodes
        times = networkx.to_numpy_array(self.aug_graph, nodelist=self.aug_nodes, weight='len')
        self.times = torch.from_numpy(times)

        # binary matrix; edge encodings of aug_edges
        self.mask = self.times > 0

        # binary matrix mask; row i encodes all aug_targets corresponding to target[i]
        self.target_mask = torch.ones((self.targets_n, self.aug_nodes_n), dtype=torch.bool)
        for i, target in enumerate(self.targets):
            mem = self.graph.nodes[target]['memory']
            loc = self.aug_node_map[(target, 0)]
            self.target_mask[i, loc:loc + mem] = 0

        # array; for an aug_node, the longest incoming edge length
        self.longest = self.times.max(dim=0).values

        self.costs = torch.tensor(self.target_features['value'])

        if verbose:
            self.print_graph_stats()

    def get_patrolling_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'attack_len': self.target_features['attack_len'],
                'value': self.target_features['value'],
                'blindness': self.target_features['blindness'],
                'edges': self.edges}
        return data

    def get_window_init_data(self):
        data = {'memory': self.target_features['memory'] + self.target_features['memory'],
                'delimiters': self.target_features['delimiters'],
                'rewards': self.target_features['rewards'],
                'edges': self.edges}
        return data

    def __repr__(self):
        return self.name or self.graph.name or self.__class__.__name__

    def print_strategy(self, strategy, style='label', precision=3):
        cprint('Strategy:', 'blue')
        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(' ', np.array2string(strategy, prefix='  '))
        else:
            for (source, ms), (target, mt) in self.aug_graph.edges():
                source_index = self.aug_node_map[(source, ms)]
                target_index = self.aug_node_map[(target, mt)]
                prob = strategy[source_index, target_index]
                if prob > 0:
                    if style == 'label':
                        print(f'  {source} ({ms})--{target} ({mt}):\t{prob:.{precision}f}')
                    elif style == 'index':
                        print(f'  {source_index}--{target_index}:\t{prob:.{precision}f}')
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')

    def print_graph_stats(self):
        cprint(f'Nodes: ({self.graph.number_of_nodes()})', 'blue')
        for node in self.graph.nodes.data():
            print('  ', node)
        cprint(f'Edges: ({self.graph.number_of_edges()})', 'blue')
        for edge in self.graph.edges.data():
            print('  ', edge)
        cprint(f'Incidence matrix', 'blue')
        with np.printoptions(precision=1, suppress=True, threshold=np.inf, linewidth=np.inf):
            print(np.array2string(networkx.to_numpy_matrix(self.graph, nodelist=self.nodes, weight='len')))
        lengths = [l for _, _, l in self.graph.edges.data('len')]
        cprint('Graph statistics:', 'blue')
        print(f'  edge length: min={min(lengths)}\tmax={max(lengths)}\tavg={np.mean(lengths):.2f}')
        print(f'  degrees:')
        if self.graph.is_directed():
            out_deg = Counter(d for n, d in self.graph.out_degree())
            in_deg = Counter(d for n, d in self.graph.in_degree())
            for (out_degree, out_count), (in_degree, in_count) in zip(out_deg.most_common(), in_deg.most_common()):
                print(f'   in={in_degree:01d}:\t{in_count}x\tout={out_degree:01d}:\t{out_count}x')
        cprint('Target features', 'blue')
        for key, features in self.target_features.items():
            cprint(f'  {key}:', 'blue')
            print('  ', features)

    def get_figure(self):
        if self.graph.name.__contains__('square_subgraph'):
            pos = {node: node for node in self.graph.nodes()}
        # elif self.graph.name.__contains__('office_building'):
        #     pos = networkx.planar_layout(self.graph)
        else:
            pos = networkx.spring_layout(self.graph)
        networkx.draw(self.graph, with_labels=True, pos=pos)
        labels = {tuple(edge): label for *edge, label in self.graph.edges.data('len')}
        networkx.draw_networkx_edge_labels(self.graph, pos=pos, edge_labels=labels, label_pos=0.3)
        # plt.savefig(os.path.join(self.results_dir, 'graph.pdf'))
        # todo: return fig binary
        # https://stackoverflow.com/questions/31492525/converting-matplotlib-png-to-base64-for-viewing-in-html-template
        # https://stackoverflow.com/questions/57222519/how-to-get-a-figure-binary-object-by-matplotlib
        return 0


def graph_loader_from_string(loader):
    dct = {'identity': lambda graph: graph,
           **graphs_dict}
    return dct[loader]


def load_graph_with_modifiers(loader, loader_params, modifiers, verbose=False):
    graph = graph_loader_from_string(loader)(**loader_params)
    modifiers = modifiers or {}
    if verbose:
        cprint('Graph preprocessing:', 'green')
    for modifier, modifier_params in modifiers.items():
        # modifier = list(modifier_dict)[0]
        # modifier_params = modifier_dict[modifier]
        if verbose:
            cprint(f' {modifier}', 'green')
        graph = graph_modifier_from_string(modifier)(graph, **modifier_params, verbose=verbose)
    if verbose:
        print('done')
    return graph


def get_node_features(graph, nodes, features):
    return {key: [graph.nodes[n].get(key, float('nan')) for n in nodes] for key in features}
