from graph_utils import Graph
from reporting import ExperimentReporting, RunReporting
from model import objectives_dict, Strategy, Game
from trainer import StrategyTrainer
from utils import set_seed, parse_params


def train_one_epoch(epoch, seed, graph, objective, objective_params, strategy_params, trainer_params, reporting_params, verbose):
    set_seed(seed, epoch)

    strategy = Strategy(graph.mask, **strategy_params)
    objective = objectives_dict[objective](graph, **objective_params)
    game = Game(strategy=strategy, objective=objective)

    report = RunReporting(epoch, **reporting_params)

    StrategyTrainer(game, report, **trainer_params, verbose=verbose).train()

    return report.dump()


@parse_params()
def main(seed, verbosity, reporting_params, epochs, graph_params, objective, objective_params, strategy_params, trainer_params):
    report = ExperimentReporting(**reporting_params, settings=locals(), mode=objectives_dict[objective].mode, verbose=(verbosity >= 1))

    graph = Graph(**graph_params, verbose=(verbosity >= 2))
    # report.add_figure(graph.get_figure())

    for epoch in range(epochs):
        metrics = train_one_epoch(epoch=epoch,
                                  seed=seed,
                                  graph=graph,
                                  objective=objective,
                                  objective_params=objective_params,
                                  strategy_params=strategy_params,
                                  trainer_params=trainer_params,
                                  reporting_params=report.params,
                                  verbose=(verbosity >= 3))
        report.update(metrics)

    report.dump()


if __name__ == '__main__':
    main()
